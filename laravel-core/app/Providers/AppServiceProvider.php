<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         *
         * https://developerhowto.com/2018/11/12/how-to-change-the-laravel-public-folder/
         *
        */
        $this->app->bind('path.public', function () {
            return base_path().'/../public';
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
